package uk.co.cablepost.furtherminecarttweaks.mixin;

import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin({AbstractMinecartEntity.class})
public class AbstractMinecartEntityMixin {

    @Inject(
            method = {"getPickBlockStack"},
            at = {@At("HEAD")},
            cancellable = true
    )
    public void getPickBlockStack(CallbackInfoReturnable<ItemStack> cir) {
        AbstractMinecartEntity.Type type = ((AbstractMinecartEntity)(Object)this).getMinecartType();

        if(type == AbstractMinecartEntity.Type.FURNACE){
            cir.setReturnValue(new ItemStack(Items.FURNACE_MINECART));
        }
    }
}
