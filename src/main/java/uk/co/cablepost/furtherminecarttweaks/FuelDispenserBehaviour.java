package uk.co.cablepost.furtherminecarttweaks;

import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.vehicle.FurnaceMinecartEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import uk.co.cablepost.furtherminecarttweaks.util.FakePlayerEntity;

import java.util.List;

public class FuelDispenserBehaviour extends FallibleItemDispenserBehavior {
    @Override
    protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
        ServerWorld world = pointer.getWorld();
        if (!world.isClient()) {
            BlockPos blockPos = pointer.getPos().offset(pointer.getBlockState().get(DispenserBlock.FACING));

            this.setSuccess(FuelDispenserBehaviour.tryAddFuelToFurnaceMinecart(world, blockPos, stack));

            if (this.isSuccess()) {
                stack.decrement(1);
            }
        }
        return stack;
    }

    private static boolean tryAddFuelToFurnaceMinecart(ServerWorld world, BlockPos blockPos, ItemStack stack){
        List<FurnaceMinecartEntity> list = world.getEntitiesByClass(FurnaceMinecartEntity.class, new Box(blockPos), EntityPredicates.EXCEPT_SPECTATOR);
        for (FurnaceMinecartEntity furnaceMinecartEntity : list) {

            FakePlayerEntity playerEntity = new FakePlayerEntity(
                    world,
                    new BlockPos(
                            furnaceMinecartEntity.getX() + 1,
                            0,
                            furnaceMinecartEntity.getZ() + 1
                    )
            );
            playerEntity.setStackInHand(Hand.MAIN_HAND, stack);

            furnaceMinecartEntity.interact(playerEntity, Hand.MAIN_HAND);

            return true;
        }

        return false;
    }
}
