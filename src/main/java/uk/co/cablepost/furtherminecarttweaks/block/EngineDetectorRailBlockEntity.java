package uk.co.cablepost.furtherminecarttweaks.block;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;

public class EngineDetectorRailBlockEntity extends BlockEntity implements ExtendedScreenHandlerFactory {

    public enum NameMatchMode {
        IGNORE,
        STARTS_WITH,
        EXACT_MATCH,
        ENDS_WITH,
        REGEX
    }

    public String nameToMatch = "";
    public NameMatchMode nameMatchMode = NameMatchMode.EXACT_MATCH;
    public Boolean latch = true;

    public EngineDetectorRailBlockEntity(BlockPos pos, BlockState state) {
        super(FurtherMinecartTweaks.ENGINE_DETECTOR_RAIL_BLOCK_ENTITY, pos, state);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        nbt.putString("nameToMatch", nameToMatch);
        nbt.putString("nameMatchMode", nameMatchMode.toString());
        nbt.putBoolean("latch", latch);

        super.writeNbt(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);

        nameToMatch = nbt.getString("nameToMatch");
        nameMatchMode = NameMatchMode.valueOf(nbt.getString("nameMatchMode"));
        latch = nbt.getBoolean("latch");
    }

    public String getNameToMatch(){
        return nameToMatch;
    }

    public NameMatchMode getNameMatchMode(){
        return nameMatchMode;
    }

    public Boolean getLatch(){
        return latch;
    }

    @Override
    public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        return new EngineDetectorRailBlockScreenHandler(syncId, playerInventory, pos, world);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity serverPlayerEntity, PacketByteBuf packetByteBuf) {
        packetByteBuf.writeString(nameToMatch);
        packetByteBuf.writeString(nameMatchMode.toString());
        packetByteBuf.writeBoolean(latch);
    }
}
