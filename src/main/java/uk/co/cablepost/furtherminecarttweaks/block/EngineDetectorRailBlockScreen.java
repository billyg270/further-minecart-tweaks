package uk.co.cablepost.furtherminecarttweaks.block;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.RenameItemC2SPacket;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.lwjgl.glfw.GLFW;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;

public class EngineDetectorRailBlockScreen extends HandledScreen<EngineDetectorRailBlockScreenHandler> {

    private static final Identifier TEXTURE = new Identifier(FurtherMinecartTweaks.MOD_ID, "textures/gui/container/engine_detector_rail.png");

    private TextFieldWidget nameToMatchField;
    private String nameToMatch = "???";
    private EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode = EngineDetectorRailBlockEntity.NameMatchMode.IGNORE;
    private Boolean latch = false;

    public EngineDetectorRailBlockScreen(EngineDetectorRailBlockScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void init() {
        super.init();

        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
        titleY -= 5;

        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.playerInventoryTitleY = -1000;

        getNameToMatch(handler);
        getNameMatchMode(handler);
        getLatch(handler);

        assert this.client != null;
        this.client.keyboard.setRepeatEvents(true);
        int i = (this.width - this.backgroundWidth) / 2;
        int j = (this.height - this.backgroundHeight) / 2;
        this.nameToMatchField = new TextFieldWidget(this.textRenderer, i + 37, j + 38, 103, 12, Text.translatable("furtherminecarttweaks.engine_detector_rail.name_to_match_field"));
        this.nameToMatchField.setFocusUnlocked(false);
        this.nameToMatchField.setEditableColor(-1);
        this.nameToMatchField.setUneditableColor(-1);
        this.nameToMatchField.setDrawsBackground(false);
        this.nameToMatchField.setMaxLength(50);
        this.nameToMatchField.setChangedListener(this::onTypeNameToMatch);
        this.nameToMatchField.setText(nameToMatch);
        this.addSelectableChild(this.nameToMatchField);
        this.setInitialFocus(this.nameToMatchField);
        this.nameToMatchField.setEditable(true);
    }

    private void onTypeNameToMatch(String newNameToMatch) {
        nameToMatch = newNameToMatch;
        sendSettingsUpdate();
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        textRenderer.draw(matrices, Text.literal("Engine name match mode:"), x + 33, y + 58, 4210752);

        if(this.nameMatchMode == EngineDetectorRailBlockEntity.NameMatchMode.REGEX){
            textRenderer.draw(matrices, Text.literal("/"), x + 23, y + 38, 4210752);
            textRenderer.draw(matrices, Text.literal("/gm"), x + 145, y + 38, 4210752);
        }

        int i = 0;
        for (EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode1 : EngineDetectorRailBlockEntity.NameMatchMode.values()) {
            textRenderer.draw(matrices, Text.translatable("furtherminecarttweaks.engine_detector_rail_mode." + nameMatchMode1.toString().toLowerCase()), x + 45, y + 70 + (i * 12), 4210752);

            if(nameMatchMode1 == this.nameMatchMode){
                RenderSystem.setShaderTexture(0, TEXTURE);
                drawTexture(matrices, x + 33, y + 70 + (i * 12), 183, 0, 7, 7);
            }

            i++;
        }

        textRenderer.draw(matrices, Text.literal("Latch"), x + 45, y + 165, 4210752);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        nameToMatchField.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    public void handledScreenTick() {
        super.handledScreenTick();
        this.nameToMatchField.tick();
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (client == null || client.player == null || client.player.isSpectator()) {
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        if(
                mouseX >= x + 33 &&
                mouseX <= x + 33 + 7
        ) {
            int i = 0;
            for (EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode1 : EngineDetectorRailBlockEntity.NameMatchMode.values()) {
                if (
                        mouseY >= (y + 70 + (i * 12)) &&
                                mouseY <= (y + 70 + (i * 12) + 7)
                ) {
                    this.nameMatchMode = nameMatchMode1;
                    sendSettingsUpdate();
                    return true;
                }
                i++;
            }
        }

        return false;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        if (keyCode == GLFW.GLFW_KEY_ESCAPE) {
            assert this.client != null;
            assert this.client.player != null;
            this.client.player.closeHandledScreen();
        }
        if (nameToMatchField.keyPressed(keyCode, scanCode, modifiers) || nameToMatchField.isActive()) {
            return true;
        }
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    private <ScreenHandler> void getNameToMatch(ScreenHandler handler) {
        if (handler instanceof EngineDetectorRailBlockScreenHandler) {
            nameToMatch = ((EngineDetectorRailBlockScreenHandler) handler).getNameToMatch();;
        }
    }

    private <ScreenHandler> void getNameMatchMode(ScreenHandler handler) {
        if (handler instanceof EngineDetectorRailBlockScreenHandler) {
            nameMatchMode = ((EngineDetectorRailBlockScreenHandler) handler).getNameMatchMode();
        }
    }

    private <ScreenHandler> void getLatch(ScreenHandler handler) {
        if (handler instanceof EngineDetectorRailBlockScreenHandler) {
            latch = ((EngineDetectorRailBlockScreenHandler) handler).getLatch();
        }
    }

    private void sendSettingsUpdate(){
        PacketByteBuf buf = PacketByteBufs.create();

        buf.writeString(nameToMatch);
        buf.writeString(nameMatchMode.toString());
        buf.writeBoolean(latch);

        ClientPlayNetworking.send(new Identifier(FurtherMinecartTweaks.MOD_ID, "update_engine_detector_rail_settings"), buf);
    }
}
