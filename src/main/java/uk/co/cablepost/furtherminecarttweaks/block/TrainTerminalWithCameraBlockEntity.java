package uk.co.cablepost.furtherminecarttweaks.block;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;

public class TrainTerminalWithCameraBlockEntity extends BlockEntity implements CommandOutput {

    private String portalName;

    public TrainTerminalWithCameraBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public TrainTerminalWithCameraBlockEntity(BlockPos pos, BlockState state) {
        this(FurtherMinecartTweaks.TRAIN_TERMINAL_WITH_CAMERA_BLOCK_ENTITY, pos, state);
    }

    @Override
    public void writeNbt(NbtCompound tag) {

        //...

        super.writeNbt(tag);
    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);

        //...
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public static <E extends BlockEntity> void clientTick(World world, BlockPos blockPos, BlockState blockState, E e) {
    }

    public static <E extends BlockEntity> void serverTick(World world, BlockPos blockPos, BlockState blockState, E e) {
    }

    private void setupPortal(ServerWorld serverWorld){
        if(!FabricLoader.getInstance().isModLoaded("immersive_portals")){
            return;
        }

        executeCommand(serverWorld, "say hello world");
    }

    private void removePortal(ServerWorld serverWorld){
        if(!FabricLoader.getInstance().isModLoaded("immersive_portals")){
            return;
        }

        executeCommand(serverWorld, "say hello world 3");
    }

    private void updatePortalDestination(ServerWorld serverWorld, World world, Vec3d pos, Quaternion rot){
        if(!FabricLoader.getInstance().isModLoaded("immersive_portals") || portalName == null){
            return;
        }

        //portal set_portal_destination_to <entity>

        executeCommand(serverWorld, "/execute as @e[name=" + portalName + "] run portal set_portal_destination " + world.getRegistryKey().getValue() + " " + pos.getX() + " " + pos.getY() + " " + pos.getZ());
        executeCommand(serverWorld, "/execute as @e[name=" + portalName + "] run portal set_portal_rotation " + rot.getX() + " " + rot.getY() + " " + rot.getZ() + " " + rot.getW());
    }

    private void executeCommand(ServerWorld serverWorld, String command){
        ServerCommandSource commandSource = getSource(serverWorld);
        CommandManager commandManager = serverWorld.getServer().getCommandManager();
        commandManager.executeWithPrefix(commandSource, command);
    }

    public ServerCommandSource getSource(ServerWorld serverWorld) {
        return new ServerCommandSource(
                this,
                Vec3d.ofCenter(TrainTerminalWithCameraBlockEntity.this.pos),
                Vec2f.ZERO,
                serverWorld,
                2,
                "TrainTerminalWithCameraBlockEntity",
                Text.of("TrainTerminalWithCameraBlockEntity"),
                serverWorld.getServer(),
                null
        );
    }

    @Override
    public void sendMessage(Text message) {

    }

    @Override
    public boolean shouldReceiveFeedback() {
        return false;
    }

    @Override
    public boolean shouldTrackOutput() {
        return false;
    }

    @Override
    public boolean shouldBroadcastConsoleToOps() {
        return true;//TODO - set to false on release
    }
}
