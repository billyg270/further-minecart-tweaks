package uk.co.cablepost.furtherminecarttweaks.block;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;

public class EngineDetectorRailBlockScreenHandler extends ScreenHandler {
    private World world;
    private BlockPos pos;
    private String nameToMatch;
    private EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode;
    private Boolean latch;

    // Called by client
    public EngineDetectorRailBlockScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
        this(syncId, playerInventory);

        nameToMatch = buf.readString();
        nameMatchMode = EngineDetectorRailBlockEntity.NameMatchMode.valueOf(buf.readString());
        latch = buf.readBoolean();
    }

    // Called by server
    public EngineDetectorRailBlockScreenHandler(int syncId, PlayerInventory playerInventory, BlockPos blockPos, World world) {
        this(syncId, playerInventory);
        pos = blockPos;
        this.world = world;
    }

    public EngineDetectorRailBlockScreenHandler(int syncId, PlayerInventory playerInventory) {
        super(
                FurtherMinecartTweaks.ENGINE_DETECTOR_RAIL_SCREEN_HANDLER,
                syncId
        );

        pos = BlockPos.ORIGIN;
        world = null;
        nameToMatch = "";
        nameMatchMode = EngineDetectorRailBlockEntity.NameMatchMode.IGNORE;
        latch = false;
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int index) {
        return null;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }

    public String getNameToMatch(){
        return nameToMatch;
    }

    public void setNameToMatch(String nameToMatch){
        if(world == null){
            return;
        }

        EngineDetectorRailBlockEntity blockEntity = (EngineDetectorRailBlockEntity)world.getBlockEntity(pos);

        assert blockEntity != null;

        blockEntity.nameToMatch = nameToMatch;
    }

    public EngineDetectorRailBlockEntity.NameMatchMode getNameMatchMode(){
        return nameMatchMode;
    }

    public void setNameMatchMode(EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode){
        if(world == null){
            return;
        }

        EngineDetectorRailBlockEntity blockEntity = (EngineDetectorRailBlockEntity)world.getBlockEntity(pos);

        assert blockEntity != null;

        blockEntity.nameMatchMode = nameMatchMode;
    }

    public Boolean getLatch(){
        return latch;
    }

    public void setLatch(Boolean latch){
        if(world == null){
            return;
        }

        EngineDetectorRailBlockEntity blockEntity = (EngineDetectorRailBlockEntity)world.getBlockEntity(pos);

        assert blockEntity != null;

        blockEntity.latch = latch;
    }
}
