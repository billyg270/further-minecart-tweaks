package uk.co.cablepost.furtherminecarttweaks.entity.vehicle.type;

import net.minecraft.block.BlockState;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import uk.co.cablepost.furtherminecarttweaks.entity.vehicle.SteamEngineMinecartEntity;
import net.fabricmc.fabric.api.object.builder.v1.entity.MinecartComparatorLogic;

public class SteamEngineMinecartType implements FurtherMinecartTweaksMinecartType, MinecartComparatorLogic<SteamEngineMinecartEntity>  {
    @Override
    public AbstractMinecartEntity createMinecartEntity(World world, double x, double y, double z) {
        return new SteamEngineMinecartEntity(world, x, y, z);
    }

    @Override
    public int getComparatorValue(SteamEngineMinecartEntity minecart, BlockState state, BlockPos pos) {
        return 0;
    }
}
