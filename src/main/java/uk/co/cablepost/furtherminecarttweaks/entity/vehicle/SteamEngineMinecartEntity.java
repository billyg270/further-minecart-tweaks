package uk.co.cablepost.furtherminecarttweaks.entity.vehicle;

import dev.cammiescorner.cammiesminecarttweaks.MinecartTweaks;
import dev.cammiescorner.cammiesminecarttweaks.common.packets.SyncChainedMinecartPacket;
import dev.cammiescorner.cammiesminecarttweaks.integration.MinecartTweaksConfig;
import dev.cammiescorner.cammiesminecarttweaks.utils.Linkable;
import dev.cammiescorner.cammiesminecarttweaks.utils.MinecartHelper;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FurnaceBlock;
import net.minecraft.block.PoweredRailBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.VehicleInventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ChunkTicketType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Unique;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.IntStream;

public class SteamEngineMinecartEntity extends AbstractMinecartEntity implements Linkable, VehicleInventory, SidedInventory {
    @Unique
    private AbstractMinecartEntity linkedParent;
    @Unique
    private AbstractMinecartEntity linkedChild;
    @Unique
    private UUID parentUuid;
    @Unique
    private UUID childUuid;

    public double pushX;
    public double pushZ;

    private boolean activated = false;

    private float throttleInput = 1f;

    private int burnTime = 0;
    private int maxBurnTime = 0;

    private int waitToApplyEngineForceOnStartTime = 20;

    @Unique
    private ChunkPos prevChunkPos;

    private BlockPos pos = this.getBlockPos();

    private DefaultedList<ItemStack> inventory = DefaultedList.ofSize(3, ItemStack.EMPTY);

    private static final TrackedData<Boolean> LIT = DataTracker.registerData(SteamEngineMinecartEntity.class, TrackedDataHandlerRegistry.BOOLEAN);

    public SteamEngineMinecartEntity(EntityType<? extends SteamEngineMinecartEntity> type, World world) {
        super(type, world);
    }

    public SteamEngineMinecartEntity(World world, double x, double y, double z) {
        super(FurtherMinecartTweaks.STEAM_ENGINE_MINECART_ENTITY, world, x, y, z);
        this.prevChunkPos = this.getChunkPos();
    }

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(LIT, false);
    }

    @Override
    public Type getMinecartType() {
        return Type.CHEST;
    }

    @Override
    public Item getItem() {
        return FurtherMinecartTweaks.STEAM_ENGINE_MINECART_ITEM;
    }


    @Override
    public ItemStack getPickBlockStack() {
        return new ItemStack(FurtherMinecartTweaks.STEAM_ENGINE_MINECART_ITEM);
    }

    @Override
    public BlockState getDefaultContainedBlock() {
        return Blocks.BLAST_FURNACE.getDefaultState().with(FurnaceBlock.FACING, Direction.NORTH).with(FurnaceBlock.LIT, isBurning());
    }

    @Override
    public int getDefaultBlockOffset() {
        return 8;
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        if (nbt.contains("Activated", NbtElement.INT_TYPE)) {
            this.activated = nbt.getInt("Activated") == 1;
        }

        if (nbt.contains("ThrottleInput", NbtElement.FLOAT_TYPE)) {
            this.throttleInput = nbt.getFloat("ThrottleInput");
        }

        if (nbt.contains("Position", NbtElement.LONG_TYPE)) {
            this.pos = BlockPos.fromLong(nbt.getLong("BlockPos"));
        }

        if (nbt.contains("ParentUuid")) {
            this.parentUuid = nbt.getUuid("ParentUuid");
        }

        if (nbt.contains("ChildUuid")) {
            this.childUuid = nbt.getUuid("ChildUuid");
        }

        //this.pushX = nbt.getDouble("PushX");
        //this.pushZ = nbt.getDouble("PushZ");

        burnTime = nbt.getShort("BurnTime");
        maxBurnTime = nbt.getShort("MaxBurnTime");

        this.readInventoryFromNbt(nbt);

        this.prevChunkPos = new ChunkPos(nbt.getLong("PrevChunkPos"));
    }

    @Override
    public NbtCompound writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        nbt.putInt("Activated", (this.activated) ? 1 : 0);
        nbt.putFloat("ThrottleInput", this.throttleInput);
        nbt.putLong("Position", this.pos.asLong());

        if (this.getLinkedParent() != null) {
            nbt.putUuid("ParentUuid", this.getLinkedParent().getUuid());
        }

        if (this.getLinkedChild() != null) {
            nbt.putUuid("ChildUuid", this.getLinkedChild().getUuid());
        }

        nbt.putDouble("PushX", this.pushX);
        nbt.putDouble("PushZ", this.pushZ);

        nbt.putShort("BurnTime", (short)burnTime);
        nbt.putShort("MaxBurnTime", (short)maxBurnTime);

        this.writeInventoryToNbt(nbt);

        nbt.putLong("PrevChunkPos", this.prevChunkPos.toLong());

        return nbt;
    }

    @Override
    public ActionResult interact(PlayerEntity player, Hand hand) {
        if (MinecartTweaksConfig.canLinkMinecarts) {
            ItemStack stack = player.getStackInHand(hand);
            if (!this.world.isClient() && player.isSneaking() && stack.isOf(Items.CHAIN)) {
                NbtCompound nbt = stack.getOrCreateNbt();
                if (nbt.contains("ParentEntity") && !this.getUuid().equals(nbt.getUuid("ParentEntity"))) {
                    nbt.remove("ParentEntity");
                    player.sendMessage(Text.translatable("furtherminecarttweaks.link_engine_first").formatted(Formatting.RED), true);
                } else {
                    nbt.putUuid("ParentEntity", this.getUuid());
                    this.world.playSound(null, this.getX(), this.getY(), this.getZ(), SoundEvents.BLOCK_CHAIN_HIT, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                }

                return ActionResult.success(true);
            }
        }

        if (!this.world.isClient() && !player.isSneaking()) {
            //applyEngineForce(player);
            return this.open(this::emitGameEvent, player);
        }

        return super.interact(player, hand);
    }

    boolean applyEngineForce(){
        if(!isBurning() || waitToApplyEngineForceOnStartTime > 0){
            this.pushX = 0;
            this.pushZ = 0;
            return false;
        }

        if (this.getLinkedChild() != null) {
            this.pushX = this.getX() - this.getLinkedChild().getX();
            this.pushZ = this.getZ() - this.getLinkedChild().getZ();

            if(throttleInput < 0){
                pushX *= -1;
                pushZ *= -1;
            }

            return true;
        }

        PlayerEntity playerEntity = world.getClosestPlayer(this, 5);

        if(playerEntity != null) {
            playerEntity.sendMessage(Text.translatable("furtherminecarttweaks.engine_needs_coupling").formatted(Formatting.RED), true);
        }

        this.pushX = 0;
        this.pushZ = 0;
        return false;
    }

    @Override
    public double getMaxSpeed(){
        if (this.getLinkedParent() != null) {
            return this.getLinkedParent().getMaxSpeed();
        } else {
            return MinecartTweaksConfig.getOtherMinecartSpeed() * Math.abs(throttleInput);
        }
    }

    @Override
    public void dropItems(DamageSource damageSource) {
        if (this.getLinkedParent() != null || this.getLinkedChild() != null) {
            this.dropStack(new ItemStack(Items.CHAIN));
        }

        super.dropItems(damageSource);
    }

    @Override
    public boolean collidesWith(Entity other) {
        if (other instanceof AbstractMinecartEntity minecart) {
            if (this.getLinkedParent() != null && !this.getLinkedParent().equals(minecart)) {
                minecart.setVelocity(this.getVelocity());
            }
        }

        float damage = MinecartTweaksConfig.minecartDamage;
        if (damage > 0.0F && !this.world.isClient() && other instanceof LivingEntity living) {
            if (living.isAlive() && !living.hasVehicle() && this.getVelocity().length() > 1.5) {
                Vec3d knockback = living.getVelocity().add(this.getVelocity().getX() * 0.9, this.getVelocity().length() * 0.2, this.getVelocity().getZ() * 0.9);
                living.setVelocity(knockback);
                living.velocityDirty = true;
                living.damage(MinecartTweaks.minecart(this), damage);
            }
        }

        return super.collidesWith(other);
    }

    protected int getFuelTime(ItemStack fuel) {
        if (fuel.isEmpty()) {
            return 0;
        }
        Integer fuelTime = FuelRegistry.INSTANCE.get(fuel.getItem());
        return fuelTime == null ? 0 : fuelTime;
    }

    @Override
    public void tick(){
        if (!this.world.isClient()) {

            if (MinecartTweaksConfig.furnaceMinecartsLoadChunks) {
                ChunkPos currentChunkPos = ChunkSectionPos.from(this).toChunkPos();
                if (isBurning()) {
                    ((ServerWorld)world).getChunkManager().addTicket(ChunkTicketType.PLAYER, currentChunkPos, 3, this.getChunkPos());
                }

                if (!currentChunkPos.equals(this.prevChunkPos) || !isBurning()) {
                    ((ServerWorld)world).getChunkManager().removeTicket(ChunkTicketType.PLAYER, this.prevChunkPos, 3, this.getChunkPos());
                }

                this.prevChunkPos = currentChunkPos;
            }

            if(waitToApplyEngineForceOnStartTime > 0){
                setVelocity(this.getVelocity().multiply(0f));
                waitToApplyEngineForceOnStartTime--;
            }

            PlayerLookup.tracking(this).forEach((player) -> {
                SyncChainedMinecartPacket.send(player, this.linkedParent, this);
            });
            if (this.getLinkedParent() != null) {
                double distance = this.getLinkedParent().distanceTo(this) - 1.0F;
                if (!(distance <= 4.0)) {
                    ((Linkable)this.getLinkedParent()).setLinkedChild(null);
                    this.setLinkedParent(null);
                    this.dropStack(new ItemStack(Items.CHAIN));
                    return;
                }

                Vec3d direction = this.getLinkedParent().getPos().subtract(this.getPos()).normalize();
                if (distance > 1.0) {
                    Vec3d parentVelocity = this.getLinkedParent().getVelocity();
                    if (parentVelocity.length() == 0.0) {
                        this.setVelocity(direction.multiply(0.05));
                    } else {
                        this.setVelocity(direction.multiply(parentVelocity.length()));
                        this.setVelocity(this.getVelocity().multiply(distance));
                    }
                } else if (distance < 0.8) {
                    this.setVelocity(direction.multiply(-0.05));
                } else {
                    this.setVelocity(Vec3d.ZERO);
                }

                if (this.getLinkedParent().isRemoved()) {
                    this.setLinkedParent(null);
                }
            } else {
                MinecartHelper.shouldSlowDown(this, this.world);
            }

            if (this.getLinkedChild() != null && this.getLinkedChild().isRemoved()) {
                this.setLinkedChild(null);
            }
        }

        super.tick();

        if (isBurning() && this.random.nextInt(2) == 0) {
            this.world.addParticle(
                    ParticleTypes.CAMPFIRE_COSY_SMOKE,
                    this.getX() + ((double)this.random.nextFloat() - 0.5),
                    this.getY() + 1.5,
                    this.getZ() + ((double)this.random.nextFloat() - 0.5),
                    0.0,
                    0.2,
                    0.0
            );
        }
    }

    @Override
    public void onActivatorRail(int x, int y, int z, boolean activated) {
        if (activated && !this.activated) {
            this.world.playSound(null, this.getX(), this.getY(), this.getZ(), FurtherMinecartTweaks.STEAM_ENGINE_WHISTLE_SOUND_EVENT, SoundCategory.NEUTRAL, 10.0F, 1.0F);
        }
        this.activated = activated;
    }

    private boolean numbersAreAcross0(double a, double b){
        return (a > 0 && b < 0) || (a < 0 && b > 0);
    }

    private Vec3d getVecAs1s(Vec3d v){
        double x;
        double y;
        double z;

        double sens = 0.001;

        if(v.x >= sens){
            x = 1;
        }
        else if (v.x <= -sens){
            x = -1;
        }
        else{
            x = 0;
        }

        if(v.y >= sens){
            y = 1;
        }
        else if (v.y <= -sens){
            y = -1;
        }
        else{
            y = 0;
        }

        if(v.z >= sens){
            z = 1;
        }
        else if (v.z <= -sens){
            z = -1;
        }
        else{
            z = 0;
        }

        return new Vec3d(x, y, z);
    }

    private boolean minecartOsGoingToSpotIam(AbstractMinecartEntity otherMinecart, double bx, double by, double bz){
        if(
            otherMinecart.getId() == getId() ||
            (linkedChild != null && linkedChild.getId() == otherMinecart.getId())
        ){
            return false;
        }

        return true;

//        Vec3d goingToPoz = new Vec3d(bx, by, bz);
//
//        Vec3d otherVel = getVecAs1s(otherMinecart.getVelocity());
//        Vec3d otherGoingToPoz = new Vec3d(
//                otherMinecart.getX() + (otherVel.x * 0.7),
//                otherMinecart.getY() + (otherVel.y * 0.7),
//                otherMinecart.getZ() + (otherVel.z * 0.7)
//        );
//
//        ((ServerWorld)this.world).spawnParticles(
//                new DustColorTransitionParticleEffect(new Vec3f(Vec3d.unpackRgb(3790560)), new Vec3f(Vec3d.unpackRgb(3790560)), 0.5f),
//                goingToPoz.x,
//                goingToPoz.y,
//                goingToPoz.z,
//                1,
//                0.0,
//                0.0,
//                0.0,
//                0.0f
//        );
//
//        ((ServerWorld)this.world).spawnParticles(
//                new DustColorTransitionParticleEffect(new Vec3f(Vec3d.unpackRgb(1710160)), new Vec3f(Vec3d.unpackRgb(1710160)), 0.5f),
//                otherGoingToPoz.x,
//                otherGoingToPoz.y,
//                otherGoingToPoz.z,
//                1,
//                0.0,
//                0.0,
//                0.0,
//                0.0f
//        );
//
//        return goingToPoz.distanceTo(otherMinecart.getPos()) < 1.2f || goingToPoz.distanceTo(otherGoingToPoz) < 1.2f;//brb
    }

    private boolean obstructingMinecartAtPoint(double bx, double by, double bz){
        double bs = 1.5f;

        List<AbstractMinecartEntity> nearbyMinecarts = world.getEntitiesByType(
                TypeFilter.instanceOf(AbstractMinecartEntity.class),
                new Box(
                        bx - bs,
                        by - bs,
                        bz - bs,
                        bx + bs,
                        by + bs,
                        bz + bs
                ),
                otherMinecart -> minecartOsGoingToSpotIam(otherMinecart, bx, by, bz)
        );

        for (AbstractMinecartEntity nearbyMinecart : nearbyMinecarts) {
            if(
                    nearbyMinecart.getId() == getId() ||
                    (linkedChild != null && linkedChild.getId() == nearbyMinecart.getId())
            ){
                continue;
            }

            //return true;

            if(!(nearbyMinecart instanceof SteamEngineMinecartEntity) || ((SteamEngineMinecartEntity)nearbyMinecart).linkedChild == null){
                return true;
            }

            if(Math.abs(nearbyMinecart.getVelocity().x) > 0.02 || Math.abs(nearbyMinecart.getVelocity().z) > 0.02) {
                return true;
            }

            if(
                (
                    numbersAreAcross0(nearbyMinecart.getVelocity().x, pushX) || numbersAreAcross0(nearbyMinecart.getVelocity().z, pushZ)
                ) && nearbyMinecart.getId() > getId()
            ){
                //moving towards each-other, and they have priority
                return true;
            }
        }

        return false;
    }

    public boolean raycastForMinecartsAhead() {
        //TODO - could use same code used to detect nearby players, excluding linked child. And perhaps UUID as a priority system if 2 engines are going for the same space

        if(linkedChild == null){
            return false;
        }

        Vec3d vel = getVecAs1s(getVelocity());

//        return
////            obstructingMinecartAtPoint(
////                getX() + ((getX() - getLinkedChild().getX()) * 0.6),
////                getY(),
////                getZ() + ((getZ() - getLinkedChild().getZ()) * 0.6)
////            )
////            ||
//            obstructingMinecartAtPoint(
//                getX() + (vel.x * 0.7),
//                getY() + (vel.y * 0.7),
//                getZ() + (vel.z * 0.7)
//            )
//            ||
//            obstructingMinecartAtPoint(
//                getX() + (vel.x * 0.4),
//                getY() + (vel.y * 0.4),
//                getZ() + (vel.z * 0.4)
//            )
//        ;

        return obstructingMinecartAtPoint(
            getX(),
            getY(),
            getZ()
        );
    }

    @Override
    protected void moveOnRail(BlockPos pos, BlockState state) {
        if (!state.isOf(Blocks.ACTIVATOR_RAIL)) {
            this.activated = false;
        }
        this.pos = pos;
        super.moveOnRail(pos, state);

        Vec3d vec3d = this.getVelocity();
        double f = vec3d.horizontalLengthSquared();
        double g = this.pushX * this.pushX + this.pushZ * this.pushZ;
        if (g > 1.0E-4 && f > 0.001) {
            double h = Math.sqrt(f);
            double i = Math.sqrt(g);
            this.pushX = vec3d.x / h * i;
            this.pushZ = vec3d.z / h * i;
        }

        boolean onUnPoweredRails = state.isOf(Blocks.POWERED_RAIL) && !state.get(PoweredRailBlock.POWERED);

        boolean minecartAhead = this.getVelocity().length() > 0.02f && raycastForMinecartsAhead();

        if (onUnPoweredRails || Math.abs(throttleInput) <= 0.001f) {
            this.pushX = 0;
            this.pushZ = 0;

            if(this.getVelocity().length() > 0.2f){
                this.world.playSound(null, this.getX(), this.getY(), this.getZ(), FurtherMinecartTweaks.STEAM_ENGINE_STEAM_RELEASE_SOUND_EVENT, SoundCategory.NEUTRAL, 0.5F, 1.0F);
            }

            this.setVelocity(this.getVelocity().normalize().multiply(0.03f));

            burnTime = 0;
            if(this.dataTracker.get(LIT)) {
                this.dataTracker.set(LIT, false);
            }
        }
        else{
            if(minecartAhead){
                this.setVelocity(this.getVelocity().normalize().multiply(0.0f));
                if(Objects.requireNonNull(getServer()).getTicks() % 20 == 0) {
                    this.world.playSound(null, this.getX(), this.getY(), this.getZ(), SoundEvents.BLOCK_BELL_USE, SoundCategory.NEUTRAL, 5.0F, 1.0F);
                }
            }
            else {
                if (applyEngineForce() && this.getVelocity().length() < 0.2f) {
                    this.world.playSound(null, this.getX(), this.getY(), this.getZ(), FurtherMinecartTweaks.STEAM_ENGINE_STEAM_RELEASE_SOUND_EVENT, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                }
            }
        }

        if (isBurning()) {
            if(!this.dataTracker.get(LIT)){
                this.dataTracker.set(LIT, true);
            }

            burnTime--;
            if(burnTime <= 0){
                this.dataTracker.set(LIT, isBurning());
            }
        }

        if((!isBurning() || burnTime <= 1) && !onUnPoweredRails && Math.abs(throttleInput) > 0.001f) {
            for (int i = 0; i < inventory.size(); i++) {
                ItemStack itemStack = getStack(i);

                if(!itemStack.isEmpty() && getFuelTime(itemStack) > 0){
                    burnTime = getFuelTime(itemStack) * 3;
                    maxBurnTime = burnTime;

                    Item item = itemStack.getItem();

                    itemStack.decrement(1);

                    if (itemStack.isEmpty()) {
                        Item item2 = item.getRecipeRemainder();
                        setStack(i, item2 == null ? ItemStack.EMPTY : new ItemStack(item2));
                    }

                    this.dataTracker.set(LIT, isBurning());

                    break;
                }
            }
        }
    }

    @Override
    protected void applySlowdown() {
        double d = this.pushX * this.pushX + this.pushZ * this.pushZ;
        if (d > 1.0E-7) {
            d = Math.sqrt(d);
            this.pushX /= d;
            this.pushZ /= d;
            Vec3d vec3d = this.getVelocity().multiply(0.8, 0.0, 0.8).add(this.pushX, 0.0, this.pushZ);
            if (this.isTouchingWater()) {
                vec3d = vec3d.multiply(0.1);
            }
            this.setVelocity(vec3d);
        } else {
            this.setVelocity(this.getVelocity().multiply(0.98, 0.0, 0.98));
        }
        super.applySlowdown();
    }

    @Override
    protected void moveOffRail() {
        this.activated = false;
        super.moveOffRail();
    }

    @Override
    public AbstractMinecartEntity getLinkedParent() {
        World var3 = this.world;
        if (var3 instanceof ServerWorld server) {
            if (this.linkedParent == null && this.parentUuid != null) {
                Entity var4 = server.getEntity(this.parentUuid);
                if (var4 instanceof AbstractMinecartEntity parent) {
                    this.setLinkedParent(parent);
                }
            }
        }

        return this.linkedParent;
    }

    @Override
    public void setLinkedParent(AbstractMinecartEntity parent) {
        this.linkedParent = parent;
        if (parent == null) {
            this.parentUuid = null;
        }
    }

    @Override
    public AbstractMinecartEntity getLinkedChild() {
        World var3 = this.world;
        if (var3 instanceof ServerWorld server) {
            if (this.linkedChild == null && this.childUuid != null) {
                Entity var4 = server.getEntity(this.childUuid);
                if (var4 instanceof AbstractMinecartEntity) {
                    AbstractMinecartEntity child = (AbstractMinecartEntity)var4;
                    this.setLinkedChild(child);
                }
            }
        }

        return this.linkedChild;
    }

    @Override
    public void setLinkedChild(AbstractMinecartEntity child) {
        this.linkedChild = child;
        if (child == null) {
            this.childUuid = null;
        }
    }

    @Nullable
    @Override
    public Identifier getLootTableId() {
        return null;
    }

    @Override
    public void setLootTableId(@Nullable Identifier lootTableId) {}

    @Override
    public long getLootTableSeed() {
        return 0;
    }

    @Override
    public void setLootTableSeed(long lootTableSeed) {}

    @Override
    public DefaultedList<ItemStack> getInventory() {
        return this.inventory;
    }

    @Override
    public void resetInventory() {
        this.inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
    }

    @Override
    public int size() {
        return 3;
    }

    @Override
    public ItemStack getStack(int slot) {
        return this.getInventoryStack(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        return this.removeInventoryStack(slot, amount);
    }

    @Override
    public ItemStack removeStack(int slot) {
        return this.removeInventoryStack(slot);
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        this.setInventoryStack(slot, stack);
    }

    @Override
    public void markDirty() {
        applyEngineForce();
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return this.canPlayerAccess(player);
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        return new SteamEngineMinecartScreenHandler(syncId, playerInventory, this, propertyDelegate);
    }

    @Override
    public void clear() {
        this.clearInventory();
    }

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0 -> {
                    return burnTime;
                }
                case 1 -> {
                    return maxBurnTime;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return 2;
        }
    };

    public boolean isBurning(){
        if(world.isClient()){
            return this.dataTracker.get(LIT);
        }

        if(burnTime > maxBurnTime){
            burnTime = maxBurnTime;
        }

        return burnTime > 0;
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        return IntStream.range(0, size()).toArray();
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return getFuelTime(stack) > 0;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return getFuelTime(stack) <= 0;
    }
}