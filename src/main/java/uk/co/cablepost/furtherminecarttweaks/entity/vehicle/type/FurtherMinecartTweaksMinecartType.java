package uk.co.cablepost.furtherminecarttweaks.entity.vehicle.type;

import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.world.World;

public interface FurtherMinecartTweaksMinecartType {
    public abstract AbstractMinecartEntity createMinecartEntity(World world, double x, double y, double z);
}
