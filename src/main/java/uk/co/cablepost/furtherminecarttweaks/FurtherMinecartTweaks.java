package uk.co.cablepost.furtherminecarttweaks;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.furtherminecarttweaks.block.*;
import uk.co.cablepost.furtherminecarttweaks.entity.vehicle.SteamEngineMinecartEntity;
import uk.co.cablepost.furtherminecarttweaks.entity.vehicle.SteamEngineMinecartScreenHandler;
import uk.co.cablepost.furtherminecarttweaks.entity.vehicle.type.SteamEngineMinecartType;
import uk.co.cablepost.furtherminecarttweaks.item.SteamEngineMinecartItem;

public class FurtherMinecartTweaks implements ModInitializer {

    public static String MOD_ID = "furtherminecarttweaks";

    public static final Item STEAM_ENGINE_MINECART_ITEM = new SteamEngineMinecartItem(
            new SteamEngineMinecartType(),
            new Item.Settings().maxCount(1).group(ItemGroup.TRANSPORTATION)
    );

    public static final EntityType<SteamEngineMinecartEntity> STEAM_ENGINE_MINECART_ENTITY = EntityType.Builder
            .<SteamEngineMinecartEntity>create(SteamEngineMinecartEntity::new, SpawnGroup.MISC).setDimensions(0.98f, 0.7f)
            .maxTrackingRange(8).build(new Identifier(MOD_ID, "steam_engine_minecart").toString());

    public static SoundEvent STEAM_ENGINE_WHISTLE_SOUND_EVENT = new SoundEvent(new Identifier(MOD_ID, "steam_engine_whistle"));
    public static SoundEvent STEAM_ENGINE_STEAM_RELEASE_SOUND_EVENT = new SoundEvent(new Identifier(MOD_ID, "steam_engine_steam_release"));

    public static final ScreenHandlerType<SteamEngineMinecartScreenHandler> STEAM_ENGINE_MINECART_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(
            new Identifier(MOD_ID, "steam_engine_minecart"),
            SteamEngineMinecartScreenHandler::new
    );

    public static final EngineDetectorRailBlock ENGINE_DETECTOR_RAIL_BLOCK = new EngineDetectorRailBlock(FabricBlockSettings.of(Material.DECORATION).strength(0.7f).noCollision().sounds(BlockSoundGroup.METAL));

    public static BlockEntityType<EngineDetectorRailBlockEntity> ENGINE_DETECTOR_RAIL_BLOCK_ENTITY;

    public static ScreenHandlerType<EngineDetectorRailBlockScreenHandler> ENGINE_DETECTOR_RAIL_SCREEN_HANDLER;

    public static final MovingMinecartDetectorRailBlock MOVING_MINECART_DETECTOR_RAIL_BLOCK = new MovingMinecartDetectorRailBlock(FabricBlockSettings.of(Material.DECORATION).strength(0.7f).noCollision().sounds(BlockSoundGroup.METAL));

    public static final Identifier TRAIN_TERMINAL_WITH_CAMERA_IDENTIFIER = new Identifier(MOD_ID, "train_terminal_with_camera");
    public TrainTerminalWithCameraBlock TRAIN_TERMINAL_WITH_CAMERA_BLOCK = new TrainTerminalWithCameraBlock(FabricBlockSettings.of(Material.METAL).strength(0.8f));
    public static BlockEntityType<TrainTerminalWithCameraBlockEntity> TRAIN_TERMINAL_WITH_CAMERA_BLOCK_ENTITY;

    @Override
    public void onInitialize() {
        //DispenserBlock.registerBehavior(Items.CHARCOAL, new FuelDispenserBehaviour());

        Registry.register(
                Registry.ITEM,
                new Identifier(MOD_ID, "steam_engine_minecart"),
                STEAM_ENGINE_MINECART_ITEM
        );

        Registry.register(
                Registry.ENTITY_TYPE,
                new Identifier(MOD_ID, "steam_engine_minecart"),
                STEAM_ENGINE_MINECART_ENTITY
        );

        Registry.register(Registry.SOUND_EVENT, new Identifier(MOD_ID, "steam_engine_whistle"), STEAM_ENGINE_WHISTLE_SOUND_EVENT);
        Registry.register(Registry.SOUND_EVENT, new Identifier(MOD_ID, "steam_engine_steam_release"), STEAM_ENGINE_STEAM_RELEASE_SOUND_EVENT);

        Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "engine_detector_rail"), ENGINE_DETECTOR_RAIL_BLOCK);
        Registry.register(Registry.ITEM, new Identifier(MOD_ID, "engine_detector_rail"), new BlockItem(ENGINE_DETECTOR_RAIL_BLOCK, new FabricItemSettings().group(ItemGroup.TRANSPORTATION)));

        ENGINE_DETECTOR_RAIL_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                new Identifier(MOD_ID, "engine_detector_rail"),
                FabricBlockEntityTypeBuilder.create(EngineDetectorRailBlockEntity::new, ENGINE_DETECTOR_RAIL_BLOCK).build()
        );

        ENGINE_DETECTOR_RAIL_SCREEN_HANDLER = ScreenHandlerRegistry.registerExtended(
                new Identifier(MOD_ID, "engine_detector_rail"),
                EngineDetectorRailBlockScreenHandler::new
        );

        ServerPlayNetworking.registerGlobalReceiver(new Identifier(MOD_ID, "update_engine_detector_rail_settings"), (server, player, handler, buf, responseSender) -> {
            String nameToMatch = buf.readString();
            EngineDetectorRailBlockEntity.NameMatchMode nameMatchMode = EngineDetectorRailBlockEntity.NameMatchMode.valueOf(buf.readString());
            Boolean latch = buf.readBoolean();

            server.executeSync(() -> {
                if (!player.isSpectator() && player.currentScreenHandler instanceof EngineDetectorRailBlockScreenHandler screenHandler) {
                    screenHandler.setNameToMatch(nameToMatch);
                    screenHandler.setNameMatchMode(nameMatchMode);
                    screenHandler.setLatch(latch);
                }
            });
        });

        Registry.register(Registry.BLOCK, new Identifier(MOD_ID, "moving_minecart_detector_rail"), MOVING_MINECART_DETECTOR_RAIL_BLOCK);
        Registry.register(Registry.ITEM, new Identifier(MOD_ID, "moving_minecart_detector_rail"), new BlockItem(MOVING_MINECART_DETECTOR_RAIL_BLOCK, new FabricItemSettings().group(ItemGroup.TRANSPORTATION)));

        TRAIN_TERMINAL_WITH_CAMERA_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                TRAIN_TERMINAL_WITH_CAMERA_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(TrainTerminalWithCameraBlockEntity::new, TRAIN_TERMINAL_WITH_CAMERA_BLOCK).build()
        );

        Registry.register(Registry.BLOCK, TRAIN_TERMINAL_WITH_CAMERA_IDENTIFIER, TRAIN_TERMINAL_WITH_CAMERA_BLOCK);
        Registry.register(Registry.ITEM, TRAIN_TERMINAL_WITH_CAMERA_IDENTIFIER, new BlockItem(TRAIN_TERMINAL_WITH_CAMERA_BLOCK, new FabricItemSettings().group(ItemGroup.TRANSPORTATION)));
    }
}
