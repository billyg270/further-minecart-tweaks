package uk.co.cablepost.furtherminecarttweaks.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.MinecartEntityRenderer;
import net.minecraft.client.render.entity.model.EntityModelLayers;
import uk.co.cablepost.furtherminecarttweaks.FurtherMinecartTweaks;
import uk.co.cablepost.furtherminecarttweaks.block.EngineDetectorRailBlockScreen;
import uk.co.cablepost.furtherminecarttweaks.entity.vehicle.SteamEngineMinecartScreen;

@Environment(EnvType.CLIENT)
public class FurtherMinecartTweaksClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(
                FurtherMinecartTweaks.STEAM_ENGINE_MINECART_ENTITY,
                (context) -> new MinecartEntityRenderer<>(context, EntityModelLayers.FURNACE_MINECART)
        );

        HandledScreens.register(FurtherMinecartTweaks.STEAM_ENGINE_MINECART_SCREEN_HANDLER, SteamEngineMinecartScreen::new);

        BlockRenderLayerMap.INSTANCE.putBlock(FurtherMinecartTweaks.ENGINE_DETECTOR_RAIL_BLOCK, RenderLayer.getTranslucent());
        HandledScreens.register(FurtherMinecartTweaks.ENGINE_DETECTOR_RAIL_SCREEN_HANDLER, EngineDetectorRailBlockScreen::new);

        BlockRenderLayerMap.INSTANCE.putBlock(FurtherMinecartTweaks.MOVING_MINECART_DETECTOR_RAIL_BLOCK, RenderLayer.getTranslucent());
    }
}
